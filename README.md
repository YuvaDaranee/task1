## Application Overview

The Flask application in this repository is a simple web application that allows users to submit data through a form. Upon submission, the data is displayed on the next page. The application also includes a basic CI/CD pipeline for automated testing and deployment.

# Flask Application CI/CD Setup and Jenkins

This repository is along with a CI/CD pipeline configured using GitLab to build and deploy and successfully built using Jenkins.

## CICD Pipeline in Gitlab

### Prerequisites
- Push your project to the gitlab repository.
- Create .gitlab-ci.yml file at the root of the repository to define CI/CD pipeline.

### Pipeline Stucture

Build:
Installs project dependencies using pip install -r requirements.txt.
Prepares the environment for testing and deployment.

Deploy:
Deploys the application to a staging environment for further testing or review.

-Add image with proper python version as image: python:3.12
-Add stages to execute the jobs as per your required sequence.

### Configuration
- Gitlab repository is configured in configure section.
- The .gitlab-ci.yml file is configured to trigger pipeline runs automatically on every changes made to yml file.

## Jenkins Build

Installation: Install Jenkins on your server or local machine.
Creating new project: Create new freestyle project by doing below configurations.
Configuration: Configure Jenkins to integrate with your GitLab repository and the branch that should be build.

Build Steps:
Install project dependencies using pip install -r requirements.txt.
Run the Flask application using python-path app.py.
python-path: path of python in your local machine.

You can see the output for the build job by clicking 'build now' option after saving your configuration section.

### output
You will localhost url to access the feedback form. You can fill the form and it will display your details in the next page.


By setting up both GitLab CI/CD and Jenkins for this project, we have established a robust automation pipeline for building and deploying the Flask application.
